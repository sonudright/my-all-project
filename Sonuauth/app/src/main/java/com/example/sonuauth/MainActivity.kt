package com.example.sonuauth

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    lateinit var auth: FirebaseAuth

    @SuppressLint("WrongViewCast", "MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth = FirebaseAuth.getInstance()


val email=findViewById<EditText>(R.id.remail)
val password=findViewById<EditText>(R.id.rpassword)
val button=findViewById<Button>(R.id.rbutton)

        button.setOnClickListener {
         auth.createUserWithEmailAndPassword(email.text.toString(),password.text.toString())

             .addOnSuccessListener {
                 val intent=Intent(this,Home_page::class.java)
                 Toast.makeText(this, "Register successful", Toast.LENGTH_SHORT).show()
                 startActivity(intent)
             }
             .addOnFailureListener {
                 Toast.makeText(this, "Register not successful", Toast.LENGTH_SHORT).show()
             }
        }

        val text=findViewById<TextView>(R.id.textview)
        text.setOnClickListener{
            val intent=Intent(this,Login::class.java)
            startActivity(intent)

        }
    }
}