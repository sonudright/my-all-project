package com.example.sonuauth

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class Login : AppCompatActivity() {
    lateinit var auth:FirebaseAuth
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth=FirebaseAuth.getInstance()

        val email= findViewById<EditText>(R.id.email1)
        val password=findViewById<EditText>(R.id.password1)
        val loginBtn=findViewById<Button>(R.id.button1)

        loginBtn.setOnClickListener {
         auth.createUserWithEmailAndPassword(email.text.toString(),password.text.toString())
             .addOnSuccessListener {
                 Toast.makeText(this, "login successful", Toast.LENGTH_SHORT).show()
                 val intent=Intent(this,Home_page::class.java)
                 startActivity(intent)
             }
             .addOnFailureListener {
                 Toast.makeText(this, "login not success", Toast.LENGTH_SHORT).show()
             }
        }


    }
}